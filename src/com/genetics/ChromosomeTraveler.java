package com.genetics;

import com.sun.xml.internal.bind.v2.runtime.reflect.Lister;
import com.transportation.*;
import com.transportation.Package;

import java.util.ArrayList;

/**
 * Created by Daniel on 08/05/2015.
 */
public class ChromosomeTraveler {

    double volumeLevel;
    double fuelLevel;
    int delivered;
    ArrayList<Package> packagesToDeliver;

    public ChromosomeTraveler(){
        fuelLevel = Chromosome.getTankCapacity();
        delivered = 0;
        volumeLevel = 0;
        delivered = 0;
        packagesToDeliver = new ArrayList<>();
    }

    public void setVolumeLevel(double volumeLevel){
        this.volumeLevel = volumeLevel;
    }

    public int getDelivered() {
        return delivered;
    }

    public double getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(double fuelLevel){
        this.fuelLevel = fuelLevel;
    }

    public double getVolumeLevel() {
        return volumeLevel;
    }

    public ArrayList<Package> getPackagesToDeliver(){
        return this.packagesToDeliver;
    }

    public void reset(){
        fuelLevel = Chromosome.getTankCapacity();
        delivered = 0;
        volumeLevel = 0;
        packagesToDeliver = new ArrayList<>();
    }

    /**
     * @param current checkpoint to deliver or pick a package, or do nothing if it's refuelling point
     * @return true
     */
    public boolean handlePackage(Checkpoint current){

        double packageVol = current.handlePackage(packagesToDeliver);

        if(packageVol == Double.MAX_VALUE)
            return false;

        if(packageVol < 0)
            delivered++;

        volumeLevel += packageVol;

        return true;
    }

    /**
     * travels from current to next and updates its fuel and volume level according to the kind of checkpoints it travels from and the distance
     * @param current
     * @param next
     * @return
     */
    public double travel(Checkpoint current, Checkpoint next){
        double fitness = current.distanceTo(next);
        fuelLevel -= Chromosome.getAverageConsumption() * fitness;

        double fuelPen = fitness * getFuelPenPercent();
        double volPen = fitness * getVolPenPercent();
        fitness += fuelPen  + volPen ;

        next.refuel(this);

        return fitness;
    }

    private double getFuelPenPercent(){
        if(fuelLevel >= 0) return 0;

        return Math.abs(fuelLevel/Chromosome.getTankCapacity());
    }

    private double getVolPenPercent(){
        if(volumeLevel <= Chromosome.getVolumeCapacity()) return 0;

        return ((volumeLevel - Chromosome.getVolumeCapacity()) / Chromosome.getVolumeCapacity());
    }


}
