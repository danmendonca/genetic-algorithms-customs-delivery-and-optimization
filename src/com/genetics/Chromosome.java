package com.genetics;

import com.customs.Main;
import com.transportation.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Daniel on 05/05/2015.
 */


public class Chromosome {
    private ArrayList<Checkpoint> genes;

    private static double AVERAGE_CONSUMPTION = 0;
    private static double TANK_CAPACITY = 0;
    private static double VOLUME_CAPACITY = 0;
    private static int DELIVERY_POINTS_COUNT = 0;
    private static double MUTATION_FACTOR = 1;

    private double fitness = 0;
    private boolean good = true;

    public static double getAverageConsumption() {
        return AVERAGE_CONSUMPTION;
    }

    public static double getTankCapacity() {
        return TANK_CAPACITY;
    }

    public static double getVolumeCapacity() {
        return VOLUME_CAPACITY;
    }

    public static int getDeliveryPointsCount() {
        return DELIVERY_POINTS_COUNT;
    }

    ChromosomeTraveler chromosomeTraveler;

    private static String chromosomeAlert = "Exception in class Chromosome. Method: ";

    /**
     *
     */
    public static void setConstants(double avgConsumption, double tankCapacity, double volumeCapacity, int deliveryPointsCount, double mutationProb) throws Exception {

        AVERAGE_CONSUMPTION = avgConsumption;
        TANK_CAPACITY = tankCapacity;
        VOLUME_CAPACITY = volumeCapacity;
        DELIVERY_POINTS_COUNT = deliveryPointsCount;
        MUTATION_FACTOR = mutationProb;

    }


    /**
     *
     */
    public Chromosome() throws Exception {

        if (AVERAGE_CONSUMPTION == 0 || TANK_CAPACITY == 0 || VOLUME_CAPACITY == 0 || DELIVERY_POINTS_COUNT == 0)
            throw new Exception("CONSTANTS NOT SET. IMPOSSIBLE TO SOLVE PROBLEM.");

        genes = new ArrayList<>();
        chromosomeTraveler = new ChromosomeTraveler();
    }

    /**
     * @param genes
     */
    public Chromosome(ArrayList<Checkpoint> genes) {
        this.genes = (ArrayList<Checkpoint>) genes.clone();
        chromosomeTraveler = new ChromosomeTraveler();
    }

    /**
     * @return
     */
    public ArrayList<Checkpoint> getGenes() {
        return genes;
    }

    /**
     * @param genes
     */
    public void setGenes(ArrayList<Checkpoint> genes) {
        this.genes = (ArrayList<Checkpoint>) genes.clone();
    }

    /**
     * @param gene
     */
    public void addGene(Checkpoint gene) {
        genes.add(gene);
    }

    /**
     * @return the 1/fitness of a cromossome.
     */
    public double getInverseFitness() {
        if (fitness > 0)
            return 1.0 / fitness;

        return 1.0 / calculateFitness();
    }

    /**
     * @return fitness of this chromosome
     */
    public double getFitness() {
        if (fitness > 0)
            return fitness;

        return calculateFitness();
    }

    /**
     * Find first position of a gene with a package equals to the checkpoints package
     *
     * @param checkpoint
     * @return index of the first checkpoint with a package
     */
    public int getFirstOfPackage(Checkpoint checkpoint) {
        int packageId = checkpoint.getPackageIdentifier();

        int index = -1;

        for (int i = 0; i < getGenes().size(); i++)
            if (getGenes().get(i).getPackageIdentifier() == packageId) {
                index = i;
                break;
            }

        return index;
    }

    /**
     * Find last position of a gene with a package equals to the checkpoints package
     *
     * @param checkpoint
     * @return index of the last checkpoint with a package
     */
    public int getLastOfPackage(Checkpoint checkpoint) {
        int packageId = checkpoint.getPackageIdentifier();

        int index = -1;

        for (int i = getGenes().size() - 1; i >= 0; i--)
            if (getGenes().get(i).getPackageIdentifier() == packageId) {
                index = i;
                break;
            }

        return index;
    }

    /**
     * Changes position of two genes with the same package as gene
     *
     * @param gene
     */
    public void changePlaces(Checkpoint gene) {
        try {


            int firstOf = getFirstOfPackage(gene);
            int lastOf = getLastOfPackage(gene);

            Checkpoint tmpGene1 = genes.get(firstOf);
            Checkpoint tmpGene2 = genes.get(lastOf);

            if (tmpGene1 != null && tmpGene2 != null) {
                genes.set(firstOf, tmpGene2);
                genes.set(lastOf, tmpGene1);
            }
        } catch (Exception e) {
            System.err.println(chromosomeAlert + "changePlaces(Checkpoint)");
            System.err.println(e.getMessage());
        }
    }

    public boolean isGood() {
        return good;
    }

    /**
     * Check and set fitness. Implies order validation of genes.
     */
    public double calculateFitness() {
        fitness = 0;
        int currentLocationIndex = 0;
        chromosomeTraveler.reset();
        good = true;

        int nrLocations = genes.size();

        for (; currentLocationIndex < nrLocations; currentLocationIndex++) {

            Checkpoint current = genes.get(currentLocationIndex);
            boolean validChromosome = chromosomeTraveler.handlePackage(current);

            if (!validChromosome) {
                validate(current);
                chromosomeTraveler.reset();
                fitness = 0;
                currentLocationIndex = -1;
                good = true;
                continue;
            }

            if (chromosomeTraveler.getDelivered() == getDeliveryPointsCount())
                return fitness;

            Checkpoint to = genes.get(currentLocationIndex + 1);
            double rawDist = current.getLocation().distanceTo(to.getLocation());

            if (Main.DEBUG) {
                ChromosomeTraveler ttmp = new ChromosomeTraveler();
                ttmp.setFuelLevel(chromosomeTraveler.getFuelLevel());
                ttmp.setVolumeLevel(chromosomeTraveler.getVolumeLevel());

                if (ttmp.travel(current, to) > rawDist)
                    System.out.print("");
            }

            double travelCost = chromosomeTraveler.travel(current, to);

            fitness += travelCost;

            if (travelCost > rawDist) {
                good = false;
            }


        }


        return fitness;
    }

    /**
     * saves this chromosome into a csv file and prints all travels from the first checkpoint to the last delivery point
     *
     * @param path where the csv will be stored
     * @return
     */
    public String printSolution(String path) {
        String out = "";
        chromosomeTraveler.reset();

        int i = 0;

        for (; i < getGenes().size(); i++) {
            Checkpoint from = getGenes().get(i);

            chromosomeTraveler.handlePackage(from);

            out += from.toString() + "\n";
            if (chromosomeTraveler.getDelivered() == Chromosome.DELIVERY_POINTS_COUNT) {
                break;
            }


            Checkpoint to = getGenes().get(i + 1);

            double distance = from.distanceTo(to);
            chromosomeTraveler.travel(from, to);

            if (chromosomeTraveler.getDelivered() != Chromosome.DELIVERY_POINTS_COUNT) {

                if (chromosomeTraveler.getFuelLevel() < 0 || chromosomeTraveler.getVolumeLevel() > Chromosome.getVolumeCapacity())
                    out += "\n\n======================== Invalid ====================================================\n";

                out += "traveled " + distance + " fuel level =" + chromosomeTraveler.getFuelLevel()
                        + " volume in use =" + chromosomeTraveler.getVolumeLevel() + "\n";

                if (chromosomeTraveler.getFuelLevel() < 0 || chromosomeTraveler.getVolumeLevel() > Chromosome.getVolumeCapacity())
                    out += "=====================================================================================\n\n";
            }
        }

        out += "\n\nfitness= " + getFitness()+"\n";
        out += (isGood()) ? "Valid chromosome" : "Invalid chromosome";

        Writer writer = null;

        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(path + "/chromosome.csv"), "utf-8"));
            for (int j = 0; j <= i; j++) {
                int identifier = getGenes().get(j).getId();
                Location loc = getGenes().get(j).getLocation();
                writer.write(identifier + ";" + loc.getLatitude() + ";" + loc.getLongitude() + ";");
                writer.write("\n");
            }
            writer.write(";;;\n;;;\n");
            writer.write(out + ";");
        } catch (IOException ex) {
            // report
        } finally {
            try {
                writer.close();
            } catch (Exception ex) {/*ignore*/}
        }

        return out;
    }


    /**
     * Resets fitness and changes the misplaced checkpoints
     *
     * @param checkpoint
     */
    private void validate(Checkpoint checkpoint) {
        changePlaces(checkpoint);
        chromosomeTraveler.reset();
    }


    /**
     * Generates 2 new Chromosomes from this and @c2
     *
     * @param c2 chromosome to crossover with
     */
    public Chromosome[] pmx(Chromosome c2) throws Exception {


        Chromosome nC1 = new Chromosome();
        Chromosome nC2 = new Chromosome();

        Random random = new Random();
        int chromosomeSize = this.getGenes().size();

        int xStart = random.nextInt(chromosomeSize - 2) + 1; //the fisrt one we do not exchange
        int xEnd = random.nextInt(chromosomeSize - xStart) + xStart;

        nC1.setGenes(this.getGenes());
        nC2.setGenes(c2.getGenes());

        pmx(xStart, xEnd, c2, nC1, nC2);


        //double delta1 = (this.getFitness() - nC1.getFitness());//double delta2 = (c2.getFitness() - nC2.getFitness());
        //nC1.mutate(delta1);//nC2.mutate(delta2);

        nC1.mutate();
        nC2.mutate();

        Chromosome cs[] = new Chromosome[2];
        cs[0] = nC1;
        cs[1] = nC2;

        return cs;
    }


    /**
     * Generates 2 new Chromosomes from this and @c2.
     * use outside pmx(c2, nC1, nC2) only for tests
     *
     * @param xStart crossover initial cut point
     * @param xEnd   crossover final cut point
     * @param c2     pairing Chromosome with whom crossover was done
     * @param nC1    where first generated chromosome is saved
     * @param nC2    where second generated chromosome is saved
     */
    public void pmx(int xStart, int xEnd, Chromosome c2, Chromosome nC1, Chromosome nC2) {

        try {

            //perform crossover
            for (int i = xStart; i <= xEnd && i < nC1.getGenes().size(); i++) {

                Checkpoint geneFrom1 = nC1.getGenes().get(i);
                Checkpoint geneFrom2 = nC2.getGenes().get(i);

                nC1.getGenes().set(i, geneFrom2);
                nC2.getGenes().set(i, geneFrom1);
            }

            //resolve repetitions
            pmxChromosomeValidator(xStart, xEnd, this, nC1);
            pmxChromosomeValidator(xStart, xEnd, c2, nC2);

        } catch (Exception e) {
            System.err.println(chromosomeAlert + "pmx with cut points");
            System.err.println(e.getMessage());
        }
    }


    /**
     * Validates(no gene occurs more than once) a generated chromosomes @nC
     *
     * @param xStart crossover initial cut point
     * @param xEnd   crossover final cut point
     * @param c      original Chromosome
     * @param nC     where a generated chromosome was saved
     */
    private void pmxChromosomeValidator(int xStart, int xEnd, Chromosome c, Chromosome nC) {

        int fstOf, lstOf, repeatedGeneIndex;

        for (int i = xStart; i <= xEnd; i++) {

            Checkpoint isRepeated = nC.getGenes().get(i);
            fstOf = nC.getGenes().indexOf(isRepeated);
            lstOf = nC.getGenes().lastIndexOf(isRepeated);

            if (fstOf != lstOf) {
                repeatedGeneIndex = fstOf != i ? fstOf : lstOf;
                solveRepetition(i, repeatedGeneIndex, xStart, xEnd, c, nC);
            }
        }
    }

    /**
     * Eliminates known repetition of a gene and those that will possibly occur in solving process
     *
     * @param currentI  position of the repeated gene and where it will stay
     * @param repeatedI position of the repeated gene to be substituted
     * @param xS        index of crossover start
     * @param xE        index of crossover end
     * @param c         original chromosome
     * @param nC1       a generated chromosome whose main parent is @c
     */
    private void solveRepetition(int currentI, int repeatedI, int xS, int xE, Chromosome c, Chromosome nC1) {
        Checkpoint oldGene = c.getGenes().get(currentI);
        nC1.getGenes().set(repeatedI, oldGene);

        int fstOf = nC1.getGenes().indexOf(oldGene);
        int lstOf = nC1.getGenes().lastIndexOf(oldGene);

        if (fstOf != lstOf) {
            if (fstOf < xS || fstOf > xE) {
                repeatedI = fstOf;
                currentI = lstOf;
            } else {
                repeatedI = lstOf;
                currentI = fstOf;
            }

            solveRepetition(currentI, repeatedI, xS, xE, c, nC1);
        }
    }


   /* public void mutate(double dt) {

        if (fixedMutationFactor) {
            fixedMutation();
            return;
        }

        if (dt >= 0 || Genome.getTemperature() == 0)
            return; //it's a better chromossome

        Random random = new Random();
        double tmpDbl = random.nextDouble();

        double exponent = dt / Genome.getTemperature();
        double probability = Math.exp(exponent);

        if (probability > tmpDbl) {
            int first = random.nextInt(getGenes().size() - 2) + 1;
            int second = random.nextInt(getGenes().size() - 2) + 1;

            Checkpoint tmp = getGenes().get(first);
            getGenes().set(first, getGenes().get(second));
            getGenes().set(second, tmp);

            this.fitness = 0; //this now maybe invalid, setting it to 0 will provoke a validation process
        }
    }*/

    public Chromosome forceMutation(Chromosome c) throws Exception {

        Chromosome bestOfTwo;
        Chromosome crossed[]= pmx(c);

        double crossed1Fit = crossed[0].getFitness(), crossed2Fit = crossed[1].getFitness();

        bestOfTwo = (crossed1Fit < crossed2Fit) ? crossed[0] : crossed[1];
        if(!bestOfTwo.isGood()) {
            if (crossed[0].isGood())
                bestOfTwo = crossed[0];
            else if(crossed[1].isGood())
                bestOfTwo = crossed[1];
        }

        return bestOfTwo;
    }

    public void forceMutation() {

        Random random = new Random();
        int first = random.nextInt(getGenes().size() - 2) + 1;
        int second = random.nextInt(getGenes().size() - 1) + 1;
        Checkpoint tmp = getGenes().get(first);


        if (tmp.getPackage() != null) //refuellingPoint
            saForceMutation(random, tmp);
        else
            if(first != 0)
                saRefuellingPointMutation(first, second, tmp);


        this.fitness = 0;
    }

    private void saForceMutation(Random random, Checkpoint tmp) {
        int firstOf = getFirstOfPackage(tmp);
        int lastOf = getLastOfPackage(tmp);

        calculateFitness();

        tmp = genes.get(firstOf);
        int order = (0.50 < random.nextDouble()) ? -1 : 1;
        int move;
        if(firstOf>0) {
            move = (order == 1) ? random.nextInt(lastOf - firstOf) : random.nextInt(firstOf);

            for (int i = 0; firstOf > 0 && firstOf < lastOf && i <= move; firstOf += order, i++) {
                if (firstOf + order == 0 || firstOf + order == lastOf)
                    break;

                genes.set(firstOf, genes.get(firstOf + order));
                genes.set(firstOf + order, tmp);
            }
        }

        tmp = genes.get(lastOf);
        order = (0.50 < random.nextDouble()) ? -1 : 1;


        move = (order == 1) ? random.nextInt(genes.size() -lastOf) : random.nextInt(lastOf - firstOf);
        for (int i = 0; lastOf > firstOf && lastOf < genes.size() - 1 && i <= move; lastOf += order) {
            if (firstOf + order == genes.size() || firstOf + order == firstOf)
                break;

            genes.set(lastOf, genes.get(lastOf + order));
            genes.set(lastOf + order, tmp);
        }
    }

    private void saRefuellingPointMutation(int first, int second, Checkpoint tmp) {
        int order = (first < second) ? 1 : -1;
        for (; first != second; first += order)
            genes.set(first, genes.get(first + order));
        genes.set(second, tmp);
    }

    private void mutate() {
        Random random = new Random();
        double tmpDbl = random.nextDouble();


        if (MUTATION_FACTOR > tmpDbl) {
            int first = random.nextInt(getGenes().size() - 2) + 1;
            int second = random.nextInt(getGenes().size() - 2) + 1;

            Checkpoint tmp = getGenes().get(first);
            getGenes().set(first, getGenes().get(second));
            getGenes().set(second, tmp);

            this.fitness = 0; //this now maybe invalid, setting it to 0 will provoke a validation process
        }
    }

    @Override
    public String toString() {
        String output = "";
        for (int i = 0; i < getGenes().size(); i++)
            output += getGenes().get(i).toString() + "\n";
        return output;
    }


}
