package com.genetics;

import com.customs.Main;
import com.transportation.Checkpoint;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Daniel on 06/05/2015.
 */
public class Genome {

    public ArrayList<Chromosome> chromosomes;

    protected static int population = 0, generationLimit = 0;
    static boolean isElitistMode = false;
    public Chromosome elitist = null;
    double sumOfInvFit = 0;
    static double isGoodEnough = 0;
    int currentGeneration = 0;
    
    //TODO: Adicionei isto...
    public int bestIt = -1;


    public ArrayList<Chromosome> getChromosomes(){
        return chromosomes;
    }

    /**
     * for testing purposes
     *
     * @param genes
     * @param population
     * @param generationLimit
     * @throws Exception
     */
    public Genome(ArrayList<Checkpoint> genes, int population, int generationLimit) throws Exception {

        this.sumOfInvFit = 0;
        this.elitist = null;
        chromosomes = new ArrayList<>();

        int genesSize = genes.size();

        if (genesSize < 4)
            throw new Exception("Not enough genes given");
        if (population < 4)
            throw new Exception("Population set too low");
        if (generationLimit < 2)
            throw new Exception("Number of generations set too low");


        this.population = population;
        this.generationLimit = generationLimit;

        Random random = new Random();
        for (int i = 0; i < population; i++) {
            populate(genes, random);
        }
    }

    /**
     * Genome constructor. Be sure to initialize constants(setConstants() call) before using.
     *
     * @param genes
     * @throws Exception if genes
     */
    public Genome(ArrayList<Checkpoint> genes) throws Exception {
        int genesSize = genes.size();

        this.sumOfInvFit = 0;
        this.elitist = null;
        chromosomes = new ArrayList<>();

        if (genesSize < 4)
            throw new Exception("Not enough genes given");
        if (population < 4)
            throw new Exception("Population set too low");
        if (generationLimit < 2)
            throw new Exception("Number of generations set too low");

        Random random = new Random();
        for (int i = 0; i < population; i++) {
            populate(genes, random);
        }
    }

    /**
     * set needed constants to enable the creation of a genome
     *
     * @param isElitistMode
     * @param population
     * @param generationLimit
     */
    public static void setConstants(boolean isElitistMode, int population, int generationLimit, double isGoodEnough) {
        Genome.isElitistMode = isElitistMode;
        Genome.population = population;
        Genome.generationLimit = generationLimit;
        Genome.isGoodEnough = isGoodEnough;
    }


    /**
     * from a set of genes, generates random chromosomes containing all of the genes once
     *
     * @param genes  a set of genes to be added into a cromossome
     * @param random a Random() object for easier memory management
     */
    private void populate(ArrayList<Checkpoint> genes, Random random) throws Exception {

        ArrayList<Checkpoint> tmpGenes = (ArrayList<Checkpoint>) genes.clone();
        Chromosome chromosome = new Chromosome();

        chromosome.addGene(tmpGenes.remove(0)); //fixed initial location

        int index;
        int tmpGenesSize;

        do {
            tmpGenesSize = tmpGenes.size() - 1;
            index = random.nextInt(tmpGenesSize + 1);

            chromosome.addGene(tmpGenes.get(index));
            tmpGenes.remove(index);

        } while (tmpGenesSize > 1);

        chromosome.addGene(tmpGenes.remove(0)); //the last location

        chromosomes.add(chromosome);
    }

    /**
     * Sums the fitness of all chromosomes, elitist included if isElitistMode
     */
    private double fitnessCalc() {
        sumOfInvFit = 0;

        if (isElitistMode && elitist != null)
            sumOfInvFit += elitist.getInverseFitness();


        for (Chromosome c : chromosomes) {
            sumOfInvFit += c.getInverseFitness();
        }

        return sumOfInvFit;
    }

    /**
     * @return a chromosome that will be used in crossover
     */
    private Chromosome selectChromosomeToCrossover() {
        double intervalMin = 0, intervalMax = 0;

        Random random = new Random();
        double p = random.nextDouble();

        if (isElitistMode && elitist != null) {
            intervalMax += elitist.getInverseFitness() / sumOfInvFit;
            if (p > intervalMin && p <= intervalMax)
                return elitist;
        }
        Chromosome selected = null;

        for (int i = 0; i < chromosomes.size(); i++) {
            Chromosome c = chromosomes.get(i);
            intervalMin = intervalMax;
            intervalMax += c.getInverseFitness() / sumOfInvFit;

            if (p > intervalMin && p <= intervalMax || i == chromosomes.size() - 1) {
                selected = c;
                break;
            }

        }
        return selected;
    }


    /**
     * @return the best chromosome in the set
     */
    public Chromosome getFittest() {
        Chromosome c = chromosomes.get(0);
        for (Chromosome eval : chromosomes)
            if (eval.getInverseFitness() > c.getInverseFitness())
                c = eval;

        return c;
    }

    public Chromosome getValidFittest(){
        Chromosome validFittest = null;

        if(isElitistMode)
            if(elitist.isGood())
                return elitist;

        for(Chromosome c : chromosomes){
            if(!c.isGood())
                continue;

            if(validFittest == null || c.getFitness() < validFittest.getFitness())
                validFittest = c;

        }

        return validFittest;
    }


    /**
     * @return the best found chromosome in generationLimit generations
     * @throws Exception
     */
    public Chromosome solve() throws Exception {
    	
    	//TODO: Adicionei isto...
        Chromosome bestFound = null;
        
        System.out.println("### Algoritmo Genético ###");
    	System.out.println("Population size = " + chromosomes.size());
    	System.out.println("Solving problem..." + System.lineSeparator());
    	
        fitnessCalc();
        double avgFit = 0, initial = 0;
       // double sAnnealingFactor = temperature / generationLimit;
        for (int i = 0; i < generationLimit; i++) {

            currentGeneration++;

            Chromosome possibleNextBest = null;
            ArrayList<Chromosome> nextGen = new ArrayList<>();

            while (nextGen.size() < chromosomes.size() - 1)
                possibleNextBest = populateNextGen(nextGen, possibleNextBest);

            chromosomes = nextGen;

            setNewElitist(possibleNextBest, nextGen);

            fitnessCalc();

            if (i == 0)
                initial = getAvgFit();

            //TODO Descomentar
            /*
            if (i != 0 && Main.DEBUG) {
                System.out.println("Current average pop. fitness: " + getAvgFit());
                System.out.println("=================================================");
                System.out.println("");
            }
            */


            Chromosome target = getValidFittest();

            if(target != null && target.getFitness() <= isGoodEnough)
                return target;
            
            //TODO: Adicionei isto...
            if(target != null){
            	if((bestFound == null) || (bestFound.getFitness() > target.getFitness())){
            		bestFound = target;
            		bestIt = i;
            	}
            }

        }

        if (Main.DEBUG) {
            double improvement = -1 * (((getAvgFit() - initial) / initial) * 100);
            System.out.println("Total improvement of " + String.format("%.2f", improvement) + "%");
            System.out.println("Average of " + getAvgFit());
            
            //TODO: Adicionei isto...
            if(bestFound != null){
            	System.out.println("=================================================");
            	System.out.println("### Best Found " + "#" + bestIt + " = " + bestFound.getFitness());
            	System.out.println("=================================================");
            }
        }

        /*
        if (isElitistMode)
            return elitist;
        */
        
        //TODO: Adicionei isto...
        if(bestFound != null){
            return bestFound;
        }
        
        return getFittest();
    }

    /**
     * Does nothing if !isElitistMode. Sets @possibleNextBest as new elitist if its fitness is less than current elitist
     * or elitist is null. Otherwise adds possibleNextBest to the nextGeneration
     *
     * @param candidate
     * @param nextGen
     */
    private void setNewElitist(Chromosome candidate, ArrayList<Chromosome> nextGen) {
        if (isElitistMode) {
            if (elitist == null || elitist.getInverseFitness() < candidate.getInverseFitness()) {
                elitist = candidate;
            } else
                nextGen.add(candidate);
        }
    }

    /**
     * Selects 2 chromosomes to crossover and then gets two new chromosomes for the next generation
     *
     * @param nextGen          a set of chromosomes generated from the crossover of two chromosomes from previous gen
     * @param possibleNextBest if !isElitistMode, is the best chromosome in the new set, else is null
     * @throws Exception
     */
    private Chromosome populateNextGen(ArrayList<Chromosome> nextGen, Chromosome possibleNextBest) throws Exception {

        Chromosome oc1 = selectChromosomeToCrossover();
        Chromosome oc2 = selectChromosomeToCrossover();
        Chromosome result[] = oc1.pmx(oc2);
        possibleNextBest = handleElitism(nextGen, possibleNextBest, result[0]);
        possibleNextBest = handleElitism(nextGen, possibleNextBest, result[1]);

        return possibleNextBest;
    }

    /**
     * @param nextGen
     * @param possibleNextBest
     * @param candidate
     * @return the chromosome with highest inverseFitness and adds the other into the arrayList
     */
    private Chromosome handleElitism(ArrayList<Chromosome> nextGen, Chromosome possibleNextBest, Chromosome candidate) {
        if (!isElitistMode) {
            nextGen.add(candidate);
            return possibleNextBest;
        }

        if (possibleNextBest == null)
            return candidate;

        if (candidate.getInverseFitness() > possibleNextBest.getInverseFitness()) {
            nextGen.add(possibleNextBest);
            return candidate;
        }

        nextGen.add(candidate);
        return possibleNextBest;
    }

    /**
     * @return average fitness per chromosome
     */
    public double getAvgFit() {
        double realSum = 0;
        double highest = 0, lowest = Double.MAX_VALUE;
        for (Chromosome c : chromosomes) {
            double cFit = c.getFitness();

            if (cFit > highest) highest = cFit;
            if (cFit < lowest) lowest = cFit;

            realSum += c.getFitness();
        }
        realSum /= chromosomes.size();
        return realSum;
    }

    public int getCurrentGeneration() {
        return currentGeneration;
    }
}
