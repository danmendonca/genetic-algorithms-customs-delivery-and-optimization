package com;

import com.genetics.Chromosome;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Daniel on 27/05/2015.
 */
public class SimulatedAnnealing {
    Chromosome chromosome = null;
    ArrayList<Chromosome> chromosomes;
    public static int iterations = 0;
    public static double temperature = Double.MAX_VALUE;
    private static final double coef = 0.99;
    private static double OPTIMAL_VALUE = 0;

    public Chromosome getChromosome() {
        return chromosome;
    }

    public SimulatedAnnealing(ArrayList<Chromosome> chromosomes) {
        this.chromosomes = chromosomes;

        for (Chromosome c : chromosomes) {
            c.calculateFitness();
            if (chromosome == null || c.getInverseFitness() > chromosome.getInverseFitness())
                chromosome = c;
        }
    }

    public static void setOptimalValue(double optimalValue) {
        SimulatedAnnealing.OPTIMAL_VALUE = optimalValue;
    }

    public int solve() {
    	
    	//TODO: Adicionei isto....
    	Chromosome bestFound = null;
    	int bestIt = 0;
    	
    	System.out.println("### Arrefecimento Simulado ###");
    	System.out.println("Population size = " + chromosomes.size());
    	System.out.println("Solving problem...");

        int i = 0;
        for (; i < iterations; i++) {
            for (Chromosome c : chromosomes) {
                try {
                    c = iterate(c);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //getFittest();

            if(chromosome.getFitness() <= OPTIMAL_VALUE)
                break;

            temperature *= coef;
            
            //TODO: Adicionei isto...
            if(bestFound == null || (bestFound.getFitness() > chromosome.getFitness())){
            	if(chromosome.isGood()){
            		bestFound = chromosome;
            		bestIt = i;
            	}
            }
            
        }

        //return i;
        return bestIt;
    }

    private Chromosome iterate(Chromosome c) throws Exception {
        Chromosome bestOftwo = null;
        Chromosome mutated; // = new Chromosome(c.getGenes());
        Random random = new Random();

        int pairChromosomeIndex = random.nextInt(chromosomes.size());
        Chromosome pairChromosome = chromosomes.get(pairChromosomeIndex);

        mutated = c.forceMutation(pairChromosome);
        updateBestChromosome(mutated);

        if (mutated.getInverseFitness() > c.getInverseFitness()) {
            bestOftwo = mutated;
        } else {
            double rndDbl = random.nextDouble();
            double dF = mutated.getInverseFitness() - c.getInverseFitness();

            double probability = Math.exp(dF / temperature);
            bestOftwo = (probability > rndDbl) ? mutated : c;
        }

        return bestOftwo;
    }

    private void updateBestChromosome(Chromosome possibleBest) {

        if (chromosome == null)
            chromosome = possibleBest;

        possibleBest.calculateFitness();

        //update chromosome to and better valid one
        if (!chromosome.isGood() && possibleBest.isGood())
            chromosome = possibleBest; //better a working solution than an impossible
        else if (chromosome.isGood() && possibleBest.isGood() && possibleBest.getInverseFitness() > chromosome.getInverseFitness()){
            chromosome = possibleBest;
        }
        else if (!chromosome.isGood() && possibleBest.getInverseFitness() > chromosome.getInverseFitness())
            chromosome = possibleBest;
    }

    public static int getIterations() {
        return iterations;
    }

    public static void setIterations(int iterations) {
        SimulatedAnnealing.iterations = iterations;
    }

    public static double getTemperature() {
        return temperature;
    }

    public static void setTemperature(double temperature) {
        SimulatedAnnealing.temperature = temperature;
    }


    public void getFittest(int numIt){
        for(Chromosome c : chromosomes)
            if(chromosome == null || (c.getInverseFitness() > chromosome.getInverseFitness() && c.isGood())){
                chromosome = c;
            }
    }


}
