package com.transportation;

import com.genetics.Chromosome;
import com.genetics.ChromosomeTraveler;

import java.util.ArrayList;

/**
 * Created by Daniel on 10/05/2015.
 */
public class RefuellingPoint extends ActionPoint {

   public RefuellingPoint(){
       super(null, new Location(0,0));
   }

    public RefuellingPoint(Location location){
        super(null, location);
    }
    /**
     * @param packagesToDeliver a set of packages that were already picked up and not yet delivered
     * @param currentVolume     current volume
     * @return 0 if picking point or a gas station. 0 if is delivery point and the package is present and packageId if the package to deliver is not present
     */
    @Override
    public int handlePackage(ArrayList<Package> packagesToDeliver, double[] currentVolume) {
        currentVolume[0]= 0;
        return 0;
    }

    @Override
    public double handlePackage(ArrayList<Package> packages) {
        return 0;
    }

    @Override
    public void refuel(ChromosomeTraveler chromosomeTraveler) {
        chromosomeTraveler.setFuelLevel(Chromosome.getTankCapacity());
    }

    @Override
    public int getPackageIdentifier() {
        return 0;
    }

    @Override
    public Package getPackage() {
        return null;
    }

    @Override
    public void setPackage(Package aPackage) {
        //nothing to do here
    }

    @Override
    public void setPackage(double volume) {
        //nothing to do here
    }


    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return "Refuelling point";
    }
}
