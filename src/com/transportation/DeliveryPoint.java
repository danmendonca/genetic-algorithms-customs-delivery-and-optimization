package com.transportation;

import com.genetics.ChromosomeTraveler;

import java.util.ArrayList;

/**
 * Created by Daniel on 04/05/2015.
 */
public class DeliveryPoint extends ActionPoint {

    public DeliveryPoint() {
        super();
    }

    public DeliveryPoint(Package aPackage, Location location) {
        super(aPackage, location);
    }


    @Override
    public int handlePackage(ArrayList<Package> packagesToDeliver, double[] currentVolume) {
        int index = packagesToDeliver.indexOf(this.getPackage());

        //TODO curentVolume unchanged outside this method. wtf
        if(index != -1) {
            currentVolume[0] = currentVolume[0] - getPackage().getVolume();
            packagesToDeliver.remove(index);
        }

        return index;

    }

    @Override
    public double handlePackage(ArrayList<Package> packages) {
        int index = packages.indexOf(getPackage());

        if(index == -1)
            return Double.MAX_VALUE;

        packages.remove(index);

        return -getPackage().getVolume();
    }

    @Override
    public void refuel(ChromosomeTraveler chromosomeTraveler) {
        //do nothing
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return "Delivering package nr " + getPackageIdentifier();
    }
}