package com.transportation;

/**
 * Created by Daniel on 03/05/2015.
 */
public class Location {
    double latitude;
    double longitude;

    public Location(double latitude, double longitude){
        setLatitude(latitude);
        setLongitude(longitude);
    }

    public Location(){
        latitude = longitude = 0;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double distanceTo(Location location){
        double distance, c1, c2;
        c1 = Math.abs(getLatitude() - location.getLatitude());
        c2 = Math.abs(getLongitude() - location.getLongitude());

        distance = Math.sqrt(c1*c1 + c2*c2);

        return distance;
    }
}
