package com.transportation;

import com.genetics.ChromosomeTraveler;

import java.util.ArrayList;

/**
 * Created by Daniel on 03/05/2015.
 */
public interface Checkpoint {

    /**
     * return checkpoint id
     */
    int getId();

    /**
     * @return a Package
     */
    Package getPackage();

    /**
     * @param location
     * @return distance from current checkpoint to location
     */
    double distanceTo(Location location);

    /**
     * @return the package identifier
     */
    int getPackageIdentifier();

    /**
     * @return this location
     */
    Location getLocation();

    /**
     * @param location to be associated with
     */
    void setLocation(Location location);

    /**
     *
     * @param checkpoint place to drive to
     * @return the distance from current checkpoint to the given
     */
    double distanceTo(Checkpoint checkpoint);


    /**
     * @param packages a set of packages that were already picked up and not yet delivered
     * @param currentVolume current volume
     * @return 0 if picking point or a gas station. 0 if is delivery point and the package is present and packageId if the package to deliver is not present
     */
    int handlePackage(ArrayList<Package> packages, double[] currentVolume);

    /**
     * Adds or removes a package from packages. Or does nothing to packages.
     * @param packages
     * @return the difference between current packages volum sum after performing action into packages to previous.
     */
    double handlePackage(ArrayList<Package> packages);

    /**
     * updates chromosomeTraveler fuelLevel
     * @param chromosomeTraveler
     */
    void refuel(ChromosomeTraveler chromosomeTraveler);

}
