package com.transportation;

import com.genetics.ChromosomeTraveler;

import java.lang.*;
import java.util.ArrayList;

/**
 * Created by Daniel on 04/05/2015.
 */
public class PickingPoint extends ActionPoint {

    public PickingPoint() {
        super();
    }

    public PickingPoint(Package aPackage, Location location) {
        super(aPackage, location);
    }


    /**
     * @param packagesToDeliver a set of packages that were already picked up and not yet delivered
     * @param volume
     * @return
     */
    @Override
    public int handlePackage(ArrayList<Package> packagesToDeliver, double[] volume) {
        //TODO curentVolume unchanged outside this method. wtf
        volume[0] = volume[0] + getPackage().getVolume();
        packagesToDeliver.add(this.getPackage());
        return 0;
    }

    @Override
    public double handlePackage(ArrayList<Package> packages) {
        packages.add(getPackage());
        return getPackage().getVolume();
    }

    @Override
    public void refuel(ChromosomeTraveler chromosomeTraveler) {
        //do nothing
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return "Picking package nr " + getPackageIdentifier();
    }
}
