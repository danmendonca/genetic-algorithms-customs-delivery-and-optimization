package com.transportation;

import java.util.ArrayList;

/**
 * Created by Daniel on 04/05/2015.
 */
public abstract class ActionPoint implements Checkpoint{

    private static int nrActionPoints=0;

    Package aPackage;
    Location location;
    int id;

    /**
     * for testing purposes
     */
    public static void resetNrActionPoints(){
        nrActionPoints = 0;
    }

    public ActionPoint(){
        this.id = ++nrActionPoints;
    }

    public ActionPoint(Package aPackage, Location location) {
        this.aPackage = aPackage;
        this.location = location;
        this.id = ++nrActionPoints;
    }


    public Package getPackage() {
        return aPackage;
    }

    public void setPackage(Package aPackage) {
        this.aPackage = aPackage;
    }

    public void setPackage(double volume){
        this.aPackage = new Package(volume);
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setLocation(double latitude, double longitude){
        this.location = new Location(latitude, longitude);
    }

    @Override
    public Location getLocation() {
        return location;
    }

    @Override
    public double distanceTo(Location location) {
        double distance = this.location.distanceTo(location);
        return distance;
    }

    @Override
    public double distanceTo(Checkpoint checkpoint) {
        return distanceTo(checkpoint.getLocation());
    }

    @Override
    public int getPackageIdentifier(){
        if(aPackage != null)
            return this.getPackage().getIdentifier();
        else
            return -1;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof  ActionPoint)
            if(this.getId()==((ActionPoint) obj).getId())
                return true;
        return false;
    }
}
