package com.transportation;

import java.util.Random;

/**
 * Created by Daniel on 03/05/2015.
 */
public class Package {

    protected static int PACKAGES = 0;
    double volume;
    int identifier;

    /**
     * for testing purposes
     */
    public static void resetPACKAGES() {
        Package.PACKAGES = 0;
    }

    public static int getPACKAGES() {
        return PACKAGES;
    }

    public Package(){
        this.volume = 0;
        Random random = new Random();
        identifier = ++PACKAGES;
    }

    public Package(double volume){
        this.volume = volume;
        Random random = new Random();
        identifier= ++PACKAGES;
    }


    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public int getIdentifier(){
        return this.identifier;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof  Package)
            if(getIdentifier()==((Package) obj).getIdentifier())
                return true;
        return false;
    }
}
