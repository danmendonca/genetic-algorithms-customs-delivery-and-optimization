package com.customs;

import com.SimulatedAnnealing;
import com.genetics.Chromosome;
import com.genetics.Genome;
import com.transportation.*;
import com.transportation.Package;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static final boolean DEBUG = true;
    static ArrayList<Checkpoint> checkpoints;
    static boolean isGenetics = false;
    public static int extraRuns = 0;
    public static int solutionIteration = 0;

    public static void main(String[] args) {
        // write your code here
        try {
            if (args.length == 0)
                configure(null);
            else
                configure(args[0]);

            Chromosome bestFoundSolution = solve();

            bestFoundSolution.calculateFitness();

            System.out.println(bestFoundSolution.toString());
            System.out.println("Solution fitness: " + bestFoundSolution.getFitness());
            System.out.println("=================================================");
            System.out.println("");

            String solPrint = (args.length > 0) ? bestFoundSolution.printSolution(args[0]) : bestFoundSolution.printSolution("./inputFiles/");

            if (DEBUG) {
                if (args.length > 0) {
                    System.out.println(solPrint);
                    System.out.println("Found in iteration : " + solutionIteration);
                }
                else
                    System.out.println(solPrint);
                System.out.println("");
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static Chromosome solve() throws Exception {
        Genome genome = new Genome(getCheckpoints());
        Chromosome solution = null;
        if(isGenetics){
            solution = genome.solve();
            //solutionIteration = genome.getCurrentGeneration();
            solutionIteration = genome.bestIt;
            return solution;
        }

        SimulatedAnnealing sa = new SimulatedAnnealing(genome.getChromosomes());
        solutionIteration = sa.solve();

        solution = sa.getChromosome();

        return solution;
    }



    public static ArrayList<Checkpoint> getCheckpoints() {
        return Main.checkpoints;
    }

    /**
     * sets constants and parameters needed to solve the problem from conf.csv and orders.csv files
     *
     * @param folderPath path to the folder (must end with '/') where orders.csv and conf.csv files are located
     * @throws Exception
     */
    private static void setConditions(String folderPath) throws Exception {
        String path;
        if (folderPath != null)
            path = folderPath + "/conf.csv";
        else
            path = "./inputFiles/conf.csv";

        Scanner in = new Scanner(new FileReader(path));

        in.useLocale(Locale.US);
        in.nextLine();
        in.useDelimiter(";");

        int popSize, maxIterations;
        double avgConsumption, tankCap, volCap, temperature = Double.MAX_VALUE, fixedMutationProb = 0, optimalValue= 0;
        boolean isElitist = false;
        String method = in.next();

        if(method.equalsIgnoreCase("SA"))
            isGenetics = false;
        else if(method.equalsIgnoreCase("GA"))
            isGenetics=true;

        popSize = in.nextInt();
        maxIterations = in.nextInt();
        optimalValue = in.nextDouble();
        avgConsumption = in.nextDouble();
        tankCap = in.nextDouble();
        volCap = in.nextDouble();

        if(isGenetics) {
            isElitist = (in.nextInt() == 1) ? true : false;
            fixedMutationProb = in.nextDouble();
        }
        else
            temperature = in.nextDouble();

        Chromosome.setConstants(avgConsumption, tankCap, volCap, Package.getPACKAGES(), fixedMutationProb);
        Genome.setConstants(isElitist, popSize, maxIterations, optimalValue);

        SimulatedAnnealing.setTemperature(temperature);
        SimulatedAnnealing.setIterations(maxIterations);
        SimulatedAnnealing.setOptimalValue(optimalValue);
    }

    public static void configure(String folderPath) throws Exception {
        String path;
        if (folderPath != null)
            path = folderPath + "/orders.csv";
        else
            path = "./inputFiles/orders.csv";

        Scanner in = new Scanner(new FileReader(path));
        in.nextLine(); //discard header line

        checkpoints = new ArrayList<>();

        while (in.hasNext())
            parseCheckpoints(in.nextLine().split(";"));

        setConditions(folderPath);

    }

    private static void parseCheckpoints(String[] split) {
        double vol = Double.parseDouble(split[0]);

        if (vol == -1)
            addRefuelPoint(split);
        else
            addPackageTransPoints(split);
    }

    private static void addPackageTransPoints(String[] split) {
        com.transportation.Package aPackage = new Package(Double.parseDouble(split[0]));
        Location l1 = new Location(Double.parseDouble(split[1]), Double.parseDouble(split[2]));
        Location l2 = new Location(Double.parseDouble(split[3]), Double.parseDouble(split[4]));

        PickingPoint pp = new PickingPoint(aPackage, l1);
        DeliveryPoint dp = new DeliveryPoint(aPackage, l2);

        checkpoints.add(pp);
        checkpoints.add(dp);
    }

    private static void addRefuelPoint(String[] split) {
        Location l = new Location(Double.parseDouble(split[1]), Double.parseDouble(split[2]));
        RefuellingPoint rp = new RefuellingPoint(l);
        checkpoints.add(rp);
    }

}
