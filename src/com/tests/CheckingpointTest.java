package com.tests;

import com.transportation.*;
import com.transportation.Package;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Daniel on 04/05/2015.
 */
public class CheckingpointTest {



    @Test
    public void testSetPackage() throws Exception {
        Package aPackage1 = new Package(30);
        Package aPackage2 = new Package(20.6);


        DeliveryPoint dp1 = new DeliveryPoint();
        PickingPoint pp1 = new PickingPoint();

        dp1.setPackage(aPackage1);
        pp1.setPackage(aPackage2);

        assertEquals(dp1.getPackage().equals(aPackage1), true);
        assertEquals(pp1.getPackage().equals(aPackage2), true);
    }

    @Test
    public void testPackageEquals(){
        Package aPackage2 = new Package(20.6);
        Package aPackage1 = new Package(30);

        Location location1 = new Location(40.5, 10);
        Location location2 = new Location(-10, 40.5);
        Location location3 = new Location(-10, -30);

        DeliveryPoint deliveryPoint1 = new DeliveryPoint(aPackage1, location2);
        PickingPoint pickingPoint2 = new PickingPoint(aPackage2, location3);
        PickingPoint pickingPoint1 = new PickingPoint(aPackage1, location1);

        boolean equals1 = deliveryPoint1.getPackage().equals(pickingPoint1.getPackage());
        boolean equals2 = deliveryPoint1.getPackage().equals(pickingPoint2.getPackage());
        assertEquals(equals1, true);
        assertEquals(equals2, false);
    }


    @Test
    public void testGetPackage() throws Exception {
        Package aPackage2 = new Package(20.6);
        Package aPackage1 = new Package(30);

        Location location1 = new Location(40.5, 10);
        Location location4 = new Location(20, -30.44);

        DeliveryPoint deliveryPoint2 = new DeliveryPoint(aPackage2, location4);
        PickingPoint pickingPoint1 = new PickingPoint(aPackage1, location1);

        assertEquals(pickingPoint1.getPackage().equals(aPackage1), true);
        assertEquals(deliveryPoint2.getPackage().equals(aPackage2), true);
    }


    @Test
    public void testGetPackageIdentifier() throws Exception {
        Package aPackage1 = new Package(30);
        Package aPackage2 = new Package(20.6);

        Location location2 = new Location(-10, 40.5);
        Location location3 = new Location(-10, -30);

        DeliveryPoint deliveryPoint1 = new DeliveryPoint(aPackage1, location2);
        PickingPoint pickingPoint2 = new PickingPoint(aPackage2, location3);

        assertEquals(aPackage1.getIdentifier(), 1);

        assertEquals(deliveryPoint1.getPackage().getIdentifier(), 1);
        assertEquals(pickingPoint2.getPackage().getIdentifier(), 2);

    }


    @Test
    public void testSetLocation() throws Exception {
        Location l1 = new Location();
        Location l2 = new Location();

        l1.setLatitude(10.5);
        l1.setLongitude(5.10);

        assertEquals(l1.getLatitude(), 10.5, 0.1);
        assertEquals(l1.getLongitude(), 5.10, 0.1);

        assertEquals(l2.getLatitude(), 0, 0.1);
        assertEquals(l2.getLongitude(), 0, 0.1);


        DeliveryPoint dp1 = new DeliveryPoint();
        dp1.setLocation(l1);

        PickingPoint pp1 = new PickingPoint();
        pp1.setLocation(l2);

        assertEquals(dp1.getLocation().distanceTo(l1), 0, 0.001);
        assertEquals(pp1.getLocation().distanceTo(l2), 0, 0.001);

    }


    @Test
    public void testGetLocation() throws Exception {
        Package aPackage2 = new Package(20.6);
        Package aPackage1 = new Package(30);

        Location location2 = new Location(-10, 40.5);
        Location location3 = new Location(-10, -30);

        DeliveryPoint deliveryPoint1 = new DeliveryPoint(aPackage1, location2);
        PickingPoint pickingPoint2 = new PickingPoint(aPackage2, location3);


        Location l1 = deliveryPoint1.getLocation();
        Location l2 = pickingPoint2.getLocation();

        assertEquals(l1.getLatitude(), location2.getLatitude(), 0.001);
        assertEquals(l1.getLongitude(), location2.getLongitude(), 0.001);

        assertEquals(l2.getLatitude(), location3.getLatitude(), 0.001);
        assertEquals(l2.getLongitude(), location3.getLongitude(), 0.001);

    }

    @Test
    public void testDistanceFrom() throws Exception {
        Location l1 = new Location();
        Location l2 = new Location(3, 3);

        //3^2+3^2 = 18
        assertEquals(l1.distanceTo(l2), Math.sqrt(18), 0.001);
        assertEquals(l2.distanceTo(l1), Math.sqrt(18), 0.001);

    }

    @Before
    public void resetIdentifiers(){
        Package.resetPACKAGES();
        ActionPoint.resetNrActionPoints();
    }

}