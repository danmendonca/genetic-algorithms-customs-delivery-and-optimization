package com.tests;

import com.genetics.Chromosome;
import com.transportation.*;
import com.transportation.Package;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Daniel on 09/05/2015.
 */


public class ChromosomeTest {

    double vol1 = 10;
    double vol2 = 15;
    double vol3 = 4.5;
    double vol4 = 0.5;

    Location loc1 = new Location(-2.5, 0);
    Location loc2 = new Location(2.5, 0);
    Location loc3 = new Location(0, -2.5);
    Location loc4 = new Location(0, 2.5);
    Location loc5 = new Location(-2.5, -2.5);
    Location loc6 = new Location(2.5, -2.5);
    Location loc7 = new Location(-2.5, 2.5);
    Location loc8 = new Location(2.5, 2.5);

    Package p1 = null;
    Package p2 = null;
    Package p3 = null;
    Package p4 = null;

    PickingPoint pp1 = null;
    PickingPoint pp2 = null;
    PickingPoint pp3 = null;
    PickingPoint pp4 = null;
    DeliveryPoint dv1 = null;
    DeliveryPoint dv2 = null;
    DeliveryPoint dv3 = null;
    DeliveryPoint dv4 = null;


    double d1_3 = loc1.distanceTo(loc3);
    double d3_4 = loc3.distanceTo(loc4);
    double d4_2 = loc4.distanceTo(loc2);
    double d2_7 = loc2.distanceTo(loc7);
    double d7_8 = loc7.distanceTo(loc8);
    double d8_6 = loc8.distanceTo(loc6);
    double d6_5 = loc6.distanceTo(loc5);

    double d2_3 = loc2.distanceTo(loc3);
    double d3_1 = loc3.distanceTo(loc1);
    double d1_4 = loc1.distanceTo(loc4);
    double d4_8 = loc4.distanceTo(loc8);
    double d8_5 = loc8.distanceTo(loc5);
    double d5_7 = loc5.distanceTo(loc7);
    double d7_6 = loc7.distanceTo(loc6);

    Chromosome nC1, nC2, nC1R, nC2R, nC1RValid, nC2RValid, c1, c2;


    @Test
    public void testCalculateFitnessWithRefuelling() {

        Location l1 = new Location(0, 0);
        Location l2 = new Location(1, 0);
        Location l3 = new Location(2, 0);
        Location l4 = new Location(3, 0);
        Location l5 = new Location(4, 0);
        Location l6 = new Location(5, 0);
        Location l7 = new Location(6, 0);
        Location l8 = new Location(7, 0);

        Location l9 = new Location(3.5, 0);


        try {
            Chromosome.setConstants(1, 3.5, 100, 4, 0);
            Chromosome cWFuel1 = new Chromosome();


            pp1 = new PickingPoint(p1, l1);
            pp2 = new PickingPoint(p2, l2);
            pp3 = new PickingPoint(p3, l3);
            pp4 = new PickingPoint(p4, l4);

            RefuellingPoint rp1 = new RefuellingPoint(l9);

            dv1 = new DeliveryPoint(p1, l5);
            dv2 = new DeliveryPoint(p2, l6);
            dv3 = new DeliveryPoint(p3, l7);
            dv4 = new DeliveryPoint(p4, l8);

            cWFuel1.addGene(pp1);
            cWFuel1.addGene(pp2);
            cWFuel1.addGene(pp3);
            cWFuel1.addGene(pp4);

            cWFuel1.addGene(rp1);

            cWFuel1.addGene(dv1);
            cWFuel1.addGene(dv2);
            cWFuel1.addGene(dv3);
            cWFuel1.addGene(dv4);

            double cWFuel1Fitness = cWFuel1.calculateFitness();
            assertEquals(7, cWFuel1Fitness, 0.01);

            RefuellingPoint rp2 = new RefuellingPoint(l5);
            cWFuel1.addGene(rp2);

            //irrelevant refuel after all deliveries
            cWFuel1Fitness = cWFuel1.calculateFitness();
            assertEquals(7, cWFuel1Fitness, 0.01);


            Chromosome cWFuel2 = new Chromosome();
            cWFuel2.addGene(pp1);
            cWFuel2.addGene(pp2);
            cWFuel2.addGene(pp3);
            cWFuel2.addGene(pp4);
            //out of fuel - penalized trip (0.5/3.5) * 1
            cWFuel2.addGene(dv1);
            //refuelling - 0 distance rp2.location = dv1.location
            cWFuel2.addGene(rp2);
            cWFuel2.addGene(dv2);
            cWFuel2.addGene(dv3);
            cWFuel2.addGene(dv4);

            double cWFuel2Fitness = cWFuel2.calculateFitness();
            assertEquals(7.142857, cWFuel2Fitness, 0.01);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }





    @Test
    public void testCalculateFitness() {
        try {
            Chromosome.setConstants(1, 200, 200, 4, 0);

            double nC1DistanceSum = d1_3 + d3_4 + d4_2 + d2_7 + d7_8 + d8_6 + d6_5;
            double nC2DistanceSum = d2_3 + d3_1 + d1_4 + d4_8 + d8_5 + d5_7 + d7_6;

            double nC1RValidDist = nC1RValid.calculateFitness();
            double nC2RValidDist = nC2RValid.calculateFitness();

            assertEquals(nC1DistanceSum, nC1RValidDist, 0.01);
            assertEquals(nC2DistanceSum, nC2RValidDist, 0.01);

            if (nC1.getGenes().size() == 0) {
                nC1.setGenes(c1.getGenes());
                nC2.setGenes(c2.getGenes());
                c1.pmx(3, 5, c2, nC1, nC2);
            }

            Chromosome.setConstants(1, 30, 50, 4, 0);

            double nc1FuelPen = ((nC1DistanceSum - 30.0) / 30.0) * d6_5;
            double nc2FuelPen = ((nC2DistanceSum - 30.0) / 30.0) * d7_6;

            double nC1Fitness = nC1.calculateFitness();
            double nC2Fitness = nC2.calculateFitness();

            double expected1 = nC1DistanceSum + nc1FuelPen;
            double expected2 = nC2DistanceSum + nc2FuelPen;

            assertEquals(expected1, nC1Fitness, 0.1);
            assertEquals(expected2, nC2Fitness, 0.1);

            Chromosome.setConstants(1, 30, 29.5, 4, 0);
            double nc1VolPen = ((30 - 29.5) / 29.5) * d2_7;
            double nc2VolPen = ((30 - 29.5) / 29.5) * d4_8;

            nC1Fitness = nC1.calculateFitness();
            nC2Fitness = nC2.calculateFitness();

            expected1 += nc1VolPen;
            expected2 += nc2VolPen;

            assertEquals(expected1, nC1Fitness, 0.1);
            assertEquals(expected2, nC2Fitness, 0.1);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }


    @Test
    public void testPmx() throws Exception {
        try {

            nC1.setGenes(c1.getGenes());
            nC2.setGenes(c2.getGenes());
            c1.pmx(3, 5, c2, nC1, nC2);


            int equals = 0;
            for (int i = 0; i < nC1R.getGenes().size(); i++) {
                Checkpoint nC1RGene = nC1R.getGenes().get(i);
                Checkpoint nC1Gene = nC1.getGenes().get(i);
                if (nC1RGene.getId() == nC1Gene.getId())
                    equals++;
            }
            assertEquals(equals, 8);


            equals = 0;
            for (int i = 0; i < nC2R.getGenes().size(); i++) {
                Checkpoint nC2RGene = nC2R.getGenes().get(i);
                Checkpoint nC2Gene = nC2.getGenes().get(i);
                if (nC2RGene.getId() == nC2Gene.getId())
                    equals++;
            }


            assertEquals(equals, 8);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }


    /**
     * Do only after testCalculateFitness
     */

    @Test
    public void testChanged() {
        try {
            if (nC1.getGenes().size() == 0) {
                nC1.setGenes(c1.getGenes());
                nC2.setGenes(c2.getGenes());
                c1.pmx(3, 5, c2, nC1, nC2);
                nC1.calculateFitness();
                nC2.calculateFitness();
            }

            int equals = 0;
            for (int i = 0; i < nC1RValid.getGenes().size(); i++) {
                Checkpoint nC1RGene = nC1RValid.getGenes().get(i);
                Checkpoint nC1Gene = nC1.getGenes().get(i);
                if (nC1RGene.getId() == nC1Gene.getId())
                    equals++;
            }
            assertEquals(equals, 8);


            equals = 0;
            for (int i = 0; i < nC2RValid.getGenes().size(); i++) {
                Checkpoint nC2RGene = nC2RValid.getGenes().get(i);
                Checkpoint nC2Gene = nC2.getGenes().get(i);
                if (nC2RGene.getId() == nC2Gene.getId())
                    equals++;
            }
            assertEquals(equals, 8);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    @Before
    public void setVariables() {
        try {
            ActionPoint.resetNrActionPoints();
            Package.resetPACKAGES();

            Chromosome.setConstants(3.5, 50, 35, 4, 0);

            c1 = new Chromosome();
            c2 = new Chromosome();

            nC1 = new Chromosome();
            nC2 = new Chromosome();

            nC1R = new Chromosome();
            nC2R = new Chromosome();

            nC1RValid = new Chromosome();
            nC2RValid = new Chromosome();
        } catch (Exception e) {
            e.printStackTrace();
        }

        p1 = new Package(vol1);
        p2 = new Package(vol2);
        p3 = new Package(vol3);
        p4 = new Package(vol4);

        pp1 = new PickingPoint(p1, loc1);
        pp2 = new PickingPoint(p2, loc2);
        pp3 = new PickingPoint(p3, loc3);
        pp4 = new PickingPoint(p4, loc4);
        dv1 = new DeliveryPoint(p1, loc5);
        dv2 = new DeliveryPoint(p2, loc6);
        dv3 = new DeliveryPoint(p3, loc7);
        dv4 = new DeliveryPoint(p4, loc8);


        c1.addGene(pp1);
        c1.addGene(pp3);
        c1.addGene(dv3);
        c1.addGene(pp4);
        c1.addGene(dv4);
        c1.addGene(dv1);
        c1.addGene(pp2);
        c1.addGene(dv2);


        c2.addGene(pp2);
        c2.addGene(pp3);
        c2.addGene(pp1);
        c2.addGene(dv2);
        c2.addGene(dv3);
        c2.addGene(pp4);
        c2.addGene(dv4);
        c2.addGene(dv1);


        nC1R.addGene(pp1);
        nC1R.addGene(pp3);
        nC1R.addGene(dv4);
        nC1R.addGene(dv2);
        nC1R.addGene(dv3);
        nC1R.addGene(pp4);
        nC1R.addGene(pp2);
        nC1R.addGene(dv1);

        nC2R.addGene(pp2);
        nC2R.addGene(pp3);
        nC2R.addGene(pp1);
        nC2R.addGene(pp4);
        nC2R.addGene(dv4);
        nC2R.addGene(dv1);
        nC2R.addGene(dv3);
        nC2R.addGene(dv2);

        nC1RValid.addGene(pp1);
        nC1RValid.addGene(pp3);
        nC1RValid.addGene(pp4);
        nC1RValid.addGene(pp2);
        nC1RValid.addGene(dv3);
        nC1RValid.addGene(dv4);
        nC1RValid.addGene(dv2);
        nC1RValid.addGene(dv1);

        nC2RValid.addGene(pp2);
        nC2RValid.addGene(pp3);
        nC2RValid.addGene(pp1);
        nC2RValid.addGene(pp4);
        nC2RValid.addGene(dv4);
        nC2RValid.addGene(dv1);
        nC2RValid.addGene(dv3);
        nC2RValid.addGene(dv2);

    }
}